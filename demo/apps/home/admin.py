from django.contrib import admin
from demo.apps.home.models import UserProfile

admin.site.register(UserProfile)