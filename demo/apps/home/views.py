# Create your views here.
from django.shortcuts import render_to_response
from django.template  import RequestContext
from demo.apps.ventas.models import producto
from demo.apps.home.forms import ContacForm,LoginForm
from django.core.mail import EmailMultiAlternatives
from django.contrib.auth import login,logout,authenticate
from django.http import HttpResponseRedirect

def index_view(request):
	return render_to_response('home/index.html',context_instance=RequestContext(request))


def about_view(request):
	 return render_to_response('home/about.html',context_instance=RequestContext(request))

def prueba_view(request):
	 mensaje="esto es un mensaje desde mi vista"
	 ctx={'msg':mensaje}
	 return render_to_response('home/prueba.html',ctx,context_instance=RequestContext(request))

def productos_view(request):
	 prod= producto.objects.filter(status=True)
	 ctx={'productos':prod}
	 return render_to_response('home/productos.html',ctx,context_instance=RequestContext(request))
	
def contacto_view(request):
		info_enviada=False
		email=""
		titulo=""
		texto=""
		if request.method=="POST":
			formulario= ContacForm(request.POST)
			if formulario.is_valid():
			 	info_enviada=True
			 	email= formulario.cleaned_data['Email']
			 	titulo= formulario.cleaned_data['Titulo']
			 	texto= formulario.cleaned_data['Texto']
			 	
			 	to_admin='moist.medina@gmail.com'
			 	html_content="informacion recebida <br><br>**Mensaje**<br><br>%s" %(texto)
			 	msg=EmailMultiAlternatives('contactos recibidos',html_content,'from@server.com',[to_admin])
			 	msg.attach_alternative(html_content, 'text/html')
			 	msg.send()
		else:	
			formulario= ContacForm()
		ctx={'form':formulario,'email':email,'titulo':titulo,'texto':texto,'info_enviada':info_enviada}
		return render_to_response('home/contacto.html',ctx,context_instance=RequestContext(request))
	
def login_view(request):
	mensaje=""
	if request.user.is_authenticated():
		return HttpResponseRedirect('/')
	else:
		 if request.method=="POST":
		 	form=LoginForm(request.POST)
		 	if form.is_valid():
		 		username= form.cleaned_data['username']
		 		password= form.cleaned_data['password']
		 		usuario= authenticate(username=username,password=password)
		 		#print type(usuario.is_active())
		 		if usuario is not None and usuario.is_active:
		 			login(request,usuario)
		 			return HttpResponseRedirect('/')
		 		else:
		 				mensaje="usuario y/o mensaje incorrecto"
		 form=LoginForm()
		 ctx={'form':form,'mensaje':mensaje}
		 return render_to_response('home/login.html',ctx,context_instance=RequestContext(request))
		
def logout_view(request):
	logout(request)
	return HttpResponseRedirect('/')	