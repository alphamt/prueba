from django.shortcuts import render_to_response
from django.template import RequestContext
from demo.apps.ventas.models import producto,cliente
from demo.apps.ventas.forms import ProductoForm
def add_product_view(request):
    if request.method=="POST":
            form=ProductoForm(request.POST)
            info="inicializando"
           # ctx={'informacion':info}
            if form.is_valid():
                nombre= form.cleaned_data['nombre']
                descripcion= form.cleaned_data['descripcion']
                p=producto()
                p.nombre=nombre
                p.descripcion=descripcion
                p.status=True
                p.save()
                info="se guardo satisfactoriamente"
               # ctx={'informacion':info}
            else:
                info="informacion con datos incorrectos"
            form=ProductoForm()
            ctx={'form':form,'informacion':info}
            return render_to_response('ventas/addproducto.html',ctx,context_instance=RequestContext(request))
    else:
            form=ProductoForm()
            ctx={'form':form}
    return render_to_response('ventas/addproducto.html',ctx,context_instance=RequestContext(request))
    